public class hw004
{
    public static void main(String[] args)
    {
        System.out.println("for 1"); // 輸出 for 1
        for (int i=1; i<10; i++)
        {
            if(i==5) // 當i等於5會跳出
                break;

            System.out.println("i="+i); // 輸出 i=1.i=2.i=3.i=4
        }
        System.out.println("for 2"); // 輸出 for 2
        for (int i=1; i<10; i++)
        {
            if(i==5) // 當i等於5會跳出
                continue;

            System.out.println("i="+i); // 輸出 i=1.i=2.i=3.i=4.i=6.i=7.i=8.i=9
        }
    }
}